import 'package:flutter/material.dart';
import '../flavor_app.dart';

void startApp(String appTitle) => runApp(MyApp(appTitle));
